import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./components/TodoList";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from "react-router-dom";

class App extends Component {
  state = {
    todos: todosList,
    input: ""
  };

  handleChange = event => {
    this.setState({ input: event.target.value });
  };

  addToDo = event => {
    if (event.keyCode === 13) {
      let newState = this.state;

      newState.todos.push({
        id: Math.floor(Math.random() * 19847530),
        userID: 1,
        title: this.state.input,
        completed: false
      });
      this.setState({
        todos: todosList,
        input: ""
      });
      event.target.value = "";
    }
  };

  clearCompleted = event => {
    let oldToDos = this.state.todos.filter(list => list.completed === false);
    this.setState({ todos: oldToDos });
  };

  toggleComplete = todo => id => {
    let newState = this.state.todos.map(function(element) {
      if (element.id === id) {
        element.completed = !element.completed;
      }
      return element;
    });
    this.setState({ todos: newState });
  };

  deleteOne = id => event => {
    let newState = this.state.todos.filter(todo => todo.id !== id);
    this.setState({ todos: newState });
  };

  render() {
    return (
      <Router>
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              autofocus
              onChange={this.handleChange}
              value={this.state.input}
              onKeyDown={this.addToDo}
            />
          </header>

          <Switch>
            <Route
              exact
              path="/all"
              render={() => (
                <TodoList
                  todos={this.state.todos}
                  //Add function props
                  deleteOne={this.deleteOne}
                  onChange={this.toggleComplete}
                />
              )}
            />
            <Route
              exact
              path="/active"
              render={() => (
                <TodoList
                  todos={this.state.todos.filter(
                    todo => todo.completed === false
                  )}
                  //Add function props
                  deleteOne={this.deleteOne}
                  onChange={this.toggleComplete}
                />
              )}
            />
            <Route
              exact
              path="/completed"
              render={() => (
                <TodoList
                  todos={this.state.todos.filter(
                    todo => todo.completed === true
                  )}
                  //Add function props
                  deleteOne={this.deleteOne}
                  onChange={this.toggleComplete}
                />
              )}
            />
          </Switch>

          <footer className="footer">
            <span className="todo-count">
              {/* <!-- This should be `0 items left` by default --> */}
              <strong>
                {
                  this.state.todos.filter(todo => todo.completed === false)
                    .length
                }
              </strong>{" "}
              item(s) left
            </span>
            <ul className="filters">
              <li>
                <NavLink exact to="/all" activeClassName="selected">
                  All
                </NavLink>
              </li>
              <li>
                <NavLink to="/active" activeClassName="selected">
                  Active
                </NavLink>
              </li>
              <li>
                <NavLink to="/completed" activeClassName="selected">
                  Completed
                </NavLink>
              </li>
            </ul>
            <button className="clear-completed" onClick={this.clearCompleted}>
              Clear completed
            </button>
          </footer>
        </section>
      </Router>
    );
  }
}

export default App;
